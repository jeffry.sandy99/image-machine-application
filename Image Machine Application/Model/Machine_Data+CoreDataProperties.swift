//
//  Machine_Data+CoreDataProperties.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 28/07/21.
//
//

import Foundation
import CoreData


extension Machine_Data {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Machine_Data> {
        return NSFetchRequest<Machine_Data>(entityName: "Machine_Data")
    }

    @NSManaged public var machine_id: UUID?
    @NSManaged public var machine_type: Int64
    @NSManaged public var machine_qrCode: Int64
    @NSManaged public var maintenance_date: Date?
    @NSManaged public var machine_name: String?
    @NSManaged public var pictures: [Data]?

}

extension Machine_Data : Identifiable {

}
