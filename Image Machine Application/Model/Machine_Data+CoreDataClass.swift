//
//  Machine_Data+CoreDataClass.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 28/07/21.
//
//

import Foundation
import CoreData

@objc(Machine_Data)
public class Machine_Data: NSManagedObject {    
    func dateToString(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
        return dateFormatter.string(from: date)
    }
}
