//
//  CollectionViewCell.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 28/07/21.
//

import Foundation
import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let identifier = "CollectionViewCell"
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
//        backgroundColor = .link
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setupUI()
    }
    
    // MARK: - Properties
    
    lazy var image: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .systemGray6
        image.layer.cornerRadius = 10
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        return image
    }()
    
    lazy var imageIndicator: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 10
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
}

// MARK: - UI Setup
extension CollectionViewCell {
    private func setupUI() {
        self.contentView.addSubview(image)
        self.contentView.addSubview(imageIndicator)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            image.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            image.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
            image.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
            imageIndicator.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10),
            imageIndicator.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            imageIndicator.widthAnchor.constraint(equalToConstant: 20),
            imageIndicator.heightAnchor.constraint(equalToConstant: 20),
        ])
        
    }
}

