//
//  DatabaseHandler.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 28/07/21.
//

import Foundation
import UIKit

class DatabaseHandler{
    static let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func getMachineData(withID id: Machine_Data, completion: @escaping (Machine_Data) -> Void){
        do{
            let data = try DatabaseHandler.context.existingObject(with: id.objectID)
            completion(data as! Machine_Data)
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
    }
    
    func deleteMachineData(_ selectedMachine: Machine_Data){
        DatabaseHandler.context.delete(selectedMachine)
        
        save()
    }
    
    func getAllMachineData(completion: @escaping ([Any]) -> Void){
        do{
            let data = try DatabaseHandler.context.fetch(Machine_Data.fetchRequest())
            completion(data)
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
    }
    
    func newImageMachineData(name: String){
        let newMachine = Machine_Data(context: DatabaseHandler.context)
        newMachine.machine_id = UUID()
        newMachine.machine_name = name
        newMachine.machine_type = Int64(Int.random(in: 0..<7))
        newMachine.machine_qrCode = Int64(Int.random(in: 1000..<10000))
        newMachine.maintenance_date = Date()
        newMachine.pictures = []
        
        save()
    }
    
    func save(){
        do{
            try DatabaseHandler.context.save()
        }catch let error as NSError{
            print(error.localizedDescription)
        }
    }
}
