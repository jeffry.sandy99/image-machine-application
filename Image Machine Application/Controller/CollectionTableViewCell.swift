//
//  CollectionTVCell.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 28/07/21.
//

import Foundation
import UIKit

class CollectionTableViewCell: UITableViewCell {
    static let identifier = "CollectionTableViewCell"
    
    var imageDelegate: ImagePicker!
    var collectionView: UICollectionView?
    var images: [Data]?
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionViewSetup()
    }

    func collectionViewSetup(){
        guard let collectionView = collectionView else {return}

        collectionView.backgroundColor = .secondarySystemBackground
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isScrollEnabled = true
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.identifier)
        self.addSubview(collectionView)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

extension CollectionTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images!.count < 10 ? images!.count + 1 : images!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        if !cell.isSelected{
            cell.imageIndicator.image = .none
        }
        if indexPath.row - images!.count == 0 {
            cell.image.image = UIImage(systemName: "plus")
            return cell
        }
        cell.image.image = UIImage(data: images![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.frame.width - 50) / 4, height: (self.frame.width - 50) / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell
//        print("Index: \(indexPath), \(cell?.isSelected)")
        if indexPath.row == images?.count {
            imageDelegate?.getIndex(index: indexPath)
        }
        else {
            if collectionView.isEditing{
                if cell!.isSelected{
                    cell?.imageIndicator.image = UIImage(systemName: "checkmark.circle.fill")
                    cell?.imageIndicator.backgroundColor = .white
                }
            }else {
                imageDelegate?.getIndex(index: indexPath)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell
        if collectionView.isEditing{
            if !cell!.isSelected{
                cell?.imageIndicator.image = .none
                cell?.imageIndicator.backgroundColor = .clear
            }
        }
    }
}
