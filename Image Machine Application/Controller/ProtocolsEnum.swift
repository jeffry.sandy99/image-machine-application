//
//  Protocols.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 29/07/21.
//

import Foundation

protocol ImagePicker {
    func getIndex(index: IndexPath)
}

protocol Barcode {
    func getBarcode(data: Machine_Data)
}

enum DetailPage: Int, CaseIterable{
    case id = 0
    case type = 1
    case maintenance_Date = 2
    case qr_Code = 3
}

enum Types: String, CaseIterable{
    case family = "Family"
    case vacation = "Vacation"
    case pet = "Pet"
    case friends = "Friends"
    case work = "Work"
    case meTime = "Me Time"
}
