//
//  ViewController.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 27/07/21.
//

import UIKit

class MainPageViewController: UIViewController {

    
    @IBOutlet weak var machineTableView: UITableView!
    @IBOutlet weak var sortButton: UIButton!
    
    var machinesData: [Machine_Data]?
    var DatabaseObject = DatabaseHandler()
    var selectedMachineData: Machine_Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        setupTableView()
        
        //get Machine Data from core data
        fetchMachineData()
        setupSortButton()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.scrollEdgeAppearance = .none
        navigationController?.navigationBar.backgroundColor = .secondarySystemBackground
        view.backgroundColor = .secondarySystemBackground
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailViewController"{
            let vc = segue.destination as? MachineDataDetailViewController
            vc?.machineData = selectedMachineData
        }
    }
    
    func setupSortButton(){
        sortButton.setTitle("Sort List", for: .normal)
        sortButton.contentHorizontalAlignment = .trailing
        sortButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
    }
    
    @IBAction func didTapSortButton(_ sender: Any) {
        let alert = UIAlertController(title: "Sort name and type", message: "How do you want to sort the name and type?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Ascending A -> Z", style: .default, handler: { _ in
//            print("Asc")
            self.machinesData = self.machinesData?.sorted(by: {$0.machine_name! < $1.machine_name!})
            self.machineTableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Descending Z -> A", style: .default, handler: { _ in
//            print("Desc")
            self.machinesData = self.machinesData?.sorted(by: {$0.machine_name! > $1.machine_name!})
            self.machineTableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
//            print("Cancel")
            alert.dismiss(animated: true, completion: nil)
        }))

        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapQRButton(_ sender: Any) {
        let vc = BarcodeScannerVC()
        vc.machinesData = machinesData
        vc.barcodeDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func didTapNewButton(_ sender: Any) {
        let alert = UIAlertController(title: "New Machine Data", message: "Please enter your Machine Data Name", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Machine Data Name"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            let machineName = alert.textFields![0].text
            
            self.DatabaseObject.newImageMachineData(name: machineName!)
            
            self.fetchMachineData()
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupTableView(){
        
        machineTableView.delegate = self
        machineTableView.dataSource = self
        machineTableView.backgroundColor = .secondarySystemBackground
    }
    
    func fetchMachineData(){
        DatabaseObject.getAllMachineData { data in
            self.machinesData = data as? [Machine_Data]
            self.machineTableView.reloadData()
        }
    }
}

extension MainPageViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return machinesData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "machineCell")
        let machineData = machinesData![indexPath.row]
        cell.textLabel?.text = machineData.machine_name
        
        for (index, value) in Types.allCases.enumerated(){
            if index == machineData.machine_type{
                cell.detailTextLabel?.text = value.rawValue
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMachineData = machinesData![indexPath.row]
        self.performSegue(withIdentifier: "toDetailViewController", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
            
            let machineToRemove = self.machinesData![indexPath.row]
            
            self.DatabaseObject.deleteMachineData(machineToRemove)
            
            self.fetchMachineData()
        }
        
        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension MainPageViewController: Barcode{
    func getBarcode(data: Machine_Data) {
        selectedMachineData = data
        self.performSegue(withIdentifier: "toDetailViewController", sender: self)
    }
}
