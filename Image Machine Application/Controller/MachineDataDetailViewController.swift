//
//  MachineDataDetailViewController.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 27/07/21.
//

import Foundation
import UIKit
import PhotosUI

class MachineDataDetailViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    var staticData: [String] = [
        "ID",
        "Type",
        "Maintenance Date",
        "Machine QR Code"
    ]
    
    var machineData: Machine_Data?
    var itemProvider: [NSItemProvider] = []
    var iterator: IndexingIterator<[NSItemProvider]>?
    var buttonState: Bool = false
    var collectionView: UICollectionView!
    var trashButton: UIButton!
    var DatabaseObject = DatabaseHandler()
    var pickerView: UIPickerView!
    var datePicker: UIDatePicker = UIDatePicker()
    var pickerData: [Types] = []
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var detailTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func fetchData(){
        DatabaseObject.getMachineData(withID: machineData!) { data in
            self.machineData = data
        }
    }
    
    @IBAction func didTapEditButton(_ sender: Any) {
        buttonState = !buttonState
        editButton.title = !buttonState ? "Edit" : "Done"
        collectionView.isEditing = buttonState
        collectionView.allowsMultipleSelection = buttonState
        collectionView.allowsMultipleSelectionDuringEditing = buttonState
        
        if !buttonState{
            //not editting mode
            trashButton.isHidden = true
            collectionView.indexPathsForSelectedItems?
                .forEach { self.collectionView.deselectItem(at: $0, animated: false)
                }
        }else {
            //editting mode
            trashButton.isHidden = false
        }
        self.collectionView.reloadData()
        self.detailTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        setupCollectioView()
        setupTrashButton()
        setupTableView()
        setupTypePickerView()
        setupDatePicker()
        view.backgroundColor = .secondarySystemBackground
        
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.scrollEdgeAppearance = .none
        navigationController?.navigationBar.backgroundColor = .secondarySystemBackground
        navigationItem.title = machineData?.machine_name
    }
    
    func setupDatePicker(){
        datePicker.preferredDatePickerStyle = .automatic
        datePicker.timeZone = TimeZone.init(abbreviation: "WIT")
        datePicker.addTarget(self, action: #selector(dateDidChange), for: .valueChanged)
    }
    
    @objc func dateDidChange(){
        machineData!.maintenance_date = datePicker.date
        DatabaseObject.save()
    }
    
    func setupTrashButton(){
        trashButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        trashButton.center = CGPoint(x: view.center.x, y: view.frame.maxY - 150)
        trashButton.contentVerticalAlignment = .fill
        trashButton.contentHorizontalAlignment = .fill
        trashButton.setImage(UIImage(systemName: "trash.circle"), for: .normal)
        trashButton.addTarget(self, action: #selector(removeImage), for: .touchUpInside)
        trashButton.isHidden = true
        view.addSubview(trashButton)
    }
    
    @objc func removeImage(){
        let alert = UIAlertController(title: "Delete Image", message: "Are you sure want to delete \(collectionView.indexPathsForSelectedItems!.count) Images?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
//            print("Removed \(self.collectionView.indexPathsForSelectedItems?.count ?? 0) Images!")
            for item in self.collectionView.indexPathsForSelectedItems!.sorted(by: {$0.row > $1.row}) {
                self.machineData!.pictures?.remove(at: item.row)
            }
            
            self.DatabaseObject.save()
            self.DatabaseObject.getMachineData(withID: self.machineData!) { data in
                self.machineData = data
                self.detailTableView.reloadData()
                self.collectionView.reloadData()
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func setupCollectioView(){
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        collectionViewFlowLayout.minimumLineSpacing = 10
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        collectionView = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: view.frame.size), collectionViewLayout: collectionViewFlowLayout)
        collectionView.isEditing = false
        collectionView.allowsMultipleSelection = false
        collectionView.allowsMultipleSelectionDuringEditing = false
    }
    
    func anotherPresentPickerView(){
        let photoLibrary = PHPhotoLibrary.shared()
        var configuration = PHPickerConfiguration(photoLibrary: photoLibrary)
        configuration.selectionLimit = 10
        let picker = PHPickerViewController(configuration: configuration)
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func presentPickerView(){
        var configuration: PHPickerConfiguration = PHPickerConfiguration()
        configuration.filter = .any(of: [.images])
        configuration.selectionLimit = 10 - machineData!.pictures!.count
        
        let picker: PHPickerViewController = PHPickerViewController(configuration: configuration)
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentPicker(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
    func setupTableView(){
        detailTableView.delegate = self
        detailTableView.dataSource = self
        detailTableView.separatorStyle = .none
        detailTableView.backgroundColor = .secondarySystemBackground
        detailTableView.estimatedRowHeight = UITableView.automaticDimension
        
        detailTableView.register(CollectionTableViewCell.self, forCellReuseIdentifier: "CollectionTableViewCell")
    }
}

extension MachineDataDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? staticData.count : 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        let longCell = UITableViewCell(style: .subtitle, reuseIdentifier: "longCell")
        let collectionCell = tableView.dequeueReusableCell(withIdentifier: CollectionTableViewCell.identifier, for: indexPath) as! CollectionTableViewCell
        let title = staticData[indexPath.row]
        
        if indexPath.section == 0{
            var data = ""
            switch indexPath.row{
            case 0: data = machineData!.machine_id!.uuidString
            case 1:
                for (index, value) in Types.allCases.enumerated(){
                    if index == machineData!.machine_type{
                        data = value.rawValue
                    }
                }
            case 2: data = machineData!.dateToString(machineData!.maintenance_date!)
            case 3: data = String(machineData!.machine_qrCode)
            default: data = "No Data Available"
            }
            
            if title == "ID"{
                longCell.textLabel?.text = title
                longCell.detailTextLabel?.font = UIFont.preferredFont(forTextStyle: .callout)
                longCell.detailTextLabel?.textColor = .secondaryLabel
                longCell.detailTextLabel?.text = data
                return longCell
            }else if title == "Type"{
                cell.accessoryType = .disclosureIndicator
            }
            if title == "Maintenance Date"{
                datePicker.date = machineData!.maintenance_date!
                cell.accessoryView = datePicker
            }else {
                cell.detailTextLabel?.text = data
            }
            
            cell.textLabel?.text = title
            return cell
        }
        else {
            collectionCell.images = machineData?.pictures
            collectionCell.collectionView = collectionView
            collectionCell.imageDelegate = self
            return collectionCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == DetailPage.type.rawValue {
                showPickerView()
            }
        }
    }
    
    func showPickerView(){
        let ac = UIAlertController(title: "Types", message: "Select your suitable types\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        ac.view.addSubview(pickerView)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            for (index, value) in self.pickerData.enumerated(){
                if value.rawValue == self.pickerData[self.pickerView.selectedRow(inComponent: 0)].rawValue{
                    self.machineData?.machine_type = Int64(index)
                    self.DatabaseObject.save()
                    self.DatabaseObject.getMachineData(withID: self.machineData!) { data in
                        self.machineData = data
                        self.detailTableView.reloadData()
                    }
                }
            }
        }))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(ac, animated: true)
    }
    
    func setupTypePickerView(){
        pickerView = UIPickerView(frame: CGRect(x: 10, y: 50, width: 250, height: 150))
        pickerView.delegate = self
        pickerView.dataSource = self
        
        pickerData = Types.allCases
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Information" : "Images"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 1 ? 350 : tableView.estimatedRowHeight
    }
}

extension MachineDataDetailViewController: PHPickerViewControllerDelegate{
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true, completion: nil)
        
        itemProvider = results.map(\.itemProvider)
        iterator = itemProvider.makeIterator()
        
        for item in itemProvider{
            if item.canLoadObject(ofClass: UIImage.self) {
                item.loadObject(ofClass: UIImage.self) { image, error in
                    if let getImage = image as? UIImage {
                        DispatchQueue.main.async {
                            self.machineData?.pictures?.append(getImage.jpegData(compressionQuality: 1.0)!)
                            self.DatabaseObject.save()
                            
                            self.DatabaseObject.getMachineData(withID: self.machineData!) { data in
                                self.machineData = data
                            }
                            self.detailTableView.reloadData()
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
}

extension MachineDataDetailViewController: ImagePicker{
    func getIndex(index: IndexPath) {
//        print("\(index.row), \(machineData?.pictures?.count)")
        if index.row == machineData!.pictures!.count{
            presentPickerView()
        }else {
            showImage(machineData!.pictures![index.row])
        }
    }
    
    func showImage(_ data: Data){
        let image = UIImage(data: data)
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview()
    }
}

extension MachineDataDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        pickerData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].rawValue
    }
}
