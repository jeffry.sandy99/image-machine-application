//
//  BarcodeScanner.swift
//  Image Machine Application
//
//  Created by Jeffry Sandy Purnomo on 29/07/21.
//

import UIKit
import Network
import AVFoundation

class BarcodeScannerVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate{
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var currentUser: String = ""
    
    var DatabaseObject = DatabaseHandler()
    var machinesData: [Machine_Data]?
    var barcodeDelegate: Barcode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Scan Barcode"
        view.backgroundColor = .white
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissVC))
        startBS()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !captureSession.isRunning {
            
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !captureSession.isRunning {
            
            captureSession.stopRunning()
        }
    }
    
    @objc func dismissVC() {
        captureSession.stopRunning()
        self.dismiss(animated: true, completion: nil)
    }
    
    func startBS(){
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr, .ean8, .ean13, .pdf417]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
    
    func found(code: String) {
        machinesData?.forEach({ machineData in
//            print("\(machineData.machine_qrCode) - \(Int64(code))")
            if machineData.machine_qrCode == Int64(code){
//                print(machineData.machine_id)
                barcodeDelegate?.getBarcode(data: machineData)
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func monitorNetwork(){
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "Network")
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied{
                DispatchQueue.main.async {
                    //                    self.myLabel.text = "Internet Connected"
                    self.view.backgroundColor = .systemGreen
                }
            }else{
                DispatchQueue.main.async {
                    //                    self.myLabel.text = "No Internet Connection"
                    self.view.backgroundColor = .systemRed
                }
            }
            monitor.start(queue: queue)
        }
    }
}
