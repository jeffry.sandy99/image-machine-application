# Welcome to Image Machine Application
This application helps user to manage their alot of image and group it based on user's desire. All of the photo inside this application is seperate file from your photo gallery and will get easily to access them also. What are you waiting for, Organized your photos now..

## Support Features:
In this Application some Features such as:
- Add new Image Machine
- Add, Delete, View and Save up to 10 Photos in each Image Machine
- Sorting Image Machine Alphabetically
- Code Reader for search Image Machine
- Edit maintenance date of Image Machine
- Edit Type of Image Machine

## Features Usage Guidance:
Here's how to use each feature :
1. Add new Image Machine
```mermaid
graph LR
  id1(User Click Add Bar Button) --> id2(Fill Textfield in Alert)
  id2(Fill Textfield in Alert) --> id3(Ok, Record saved in local storage)
```

2. Add Photo
```mermaid
graph LR
  id1(User open desired Image Machine) --> id2(Click + at the bottom)
  id2(Click + at the bottom) --> id3(User select photo from popup view)
  id3(User select photo from popup view) --> id4(Click Add, Photo saved in local storage)
```

3. Delete Photo
```mermaid
graph TD
  id1(User open desired Image Machine) --> id2(Click edit on bar button)
  id2(Click edit on bar button) --> id3(Select multiple photos to delete locally)
  id3(Select multiple photos to delete locally) --> id4(Click trash button on the middle bottom)
  id4(Click trash button on the middle bottom) --> id5(Photos removed locally)
```

4. View Photo
```mermaid
graph LR
  id1(Click thumbnails at the bottom) --> id2(Image will show fullscreent)
  id2(Image will show fullscreent) --> id3(Press screen again to back)
```

5. Sorting Image List
```mermaid
graph LR
  id1(On Home Screen, click sort button) --> id2(Click 1 of menu from popup actionsheet)
  id2(Click 1 of menu from popup actionsheet) --> id3(Image Machine will sort temporary)
```

6. Code Reader
```mermaid
graph TD
  id1(Click Barcode button on bar button) --> id2(App will ask for permission)
  id2(App will ask for permission) --> id3(Camera will show up and ready to scan)
  id3(Camera will show up and ready to scan) --> id4(Code Match to one of Image Machine)
  id4(Code Match to one of Image Machine) --> id5(Navigate Automatically to DetailPage)
```

7. Edit Maintenance Date
```mermaid
graph LR
  id1(Click Maintenance Date Row) --> id2(Pick a date and time)
  id2(Pick a date and time) --> id3(Automatically save to local storage)
```

8. Edit Type
```mermaid
graph LR
  id1(Click Type Row) --> id2(Scroll Picker will Appear)
  id2(Scroll Picker will Appear) --> id3(Select 1 and click OK, saved automatically to local storage)
```
